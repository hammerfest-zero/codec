"use strict"

function Jump(nb) {
  this.nb = nb
}

function LoaderCodec(key) {
  this.key = key
  this.ikey = 0
  for (const i in key) {
    this.ikey *= 51
    this.ikey += key.charCodeAt(i)
    this.ikey &= -1
  }
}

LoaderCodec.prototype.crcStr = function () {
  return LoaderCodec.base64[this.crc & 63]
    + LoaderCodec.base64[(this.crc >> 6) & 63]
    + LoaderCodec.base64[(this.crc >> 12) & 63]
    + LoaderCodec.base64[(this.crc >> 18) & 63]
    + LoaderCodec.base64[(this.crc >> 24) & 63]
}

LoaderCodec.prototype.encode = function (o) {
  this.pkey = 0
  this.crc = 0
  this.str = ''
  this.do_encode(o)
  this.str += this.crcStr()
  return this.str
}

LoaderCodec.prototype.decode = function (s) {
  this.pkey = 0
  this.crc = 0
  this.str = s.slice(0, -5)
  //TODO: check crc
  const dec = this.do_decode(s)
  return dec
}

LoaderCodec.prototype.write = function (s) {
  for (const c of String(s)) {
    this.write_byte(c)
  }
}

LoaderCodec.prototype.write_byte = function (c) {
  const b = LoaderCodec.base64.indexOf(c)
  if (b == -1) {
    this.str += c
    return undefined
  }
  const enc = b ^ this.ikey >> this.pkey & 63
  this.crc *= 51
  this.crc += enc
  this.crc ^= -1
  this.str += LoaderCodec.base64[enc]
  this.pkey += 6
  this.pkey %= 28
}

LoaderCodec.prototype.read_byte = function () {
  const b = this.str.charAt(0)
  let dec = LoaderCodec.base64.indexOf(b)
  this.str = this.str.substring(1)
  if (dec == -1) {
    return b
  }
  this.crc *= 51
  this.crc += dec
  this.crc ^= -1
  dec ^= this.ikey >> this.pkey & 63
  this.pkey += 6
  this.pkey %= 28
  return LoaderCodec.base64[dec]
}

LoaderCodec.prototype.decode_string = function () {
  let str = ''
  while (true) {
    const b = this.read_byte()
    if (b == ':') {
      break
    }
    str += b
  }
  return str
}

LoaderCodec.prototype.encode_array = function (o) {
  this.write(o.length)
  this.write(':')
  let empty = 0
  for (let i = 0; i < o.length; ++i) {
    if (o[i] == null) {
      ++empty
    } else {
      if (empty > 1) {
        this.write('j' + empty + ':')
        empty = 0
      } else if (empty == 1) {
        this.do_encode(null)
        empty = 0
      }
      this.do_encode(o[i])
    }
  }
  if (empty > 0) {
    this.write('j' + empty + ':')
  }
}

LoaderCodec.prototype.do_encode = function (o) {
  if (o == null) {
    this.write('u')
  } else if (o instanceof Array) {
    this.write('a')
    this.encode_array(o)
  } else {
    switch (typeof o) {
      case 'string':
        this.write('s')
        this.write(o)
        this.write(':')
        break
      case 'number':
        let nb = o
        if (isNaN(nb)) {
          this.write('x')
        } else if (nb == Infinity) {
          this.write('I')
        } else if (nb == -Infinity) {
          this.write('i')
        } else {
          nb = parseInt(nb)
          this.write('n')
          if (nb < 0) {
            this.write('u')
          }
          this.write(Math.abs(nb))
          this.write(':')
        }
        break
      case 'boolean':
        this.write(o === true ? 't' : 'f')
        break
      default:
        this.write('o')
        for (const key in o) {
          this.write(key)
          this.write(':')
          this.do_encode(o[key])
        }
        this.write(':')
    }
  }
}

LoaderCodec.prototype.decode_array = function () {
  const len = parseInt(this.decode_string())
  const arr = Array(len)
  for (let i = 0; i < len; ++i) {
    const dec = this.do_decode()
    if (dec instanceof Jump)
      i += dec.nb - 1
    else
      arr[i] = dec
  }
  return arr
}

LoaderCodec.prototype.decode_object = function () {
  const obj = {}
  while (true) {
    const key = this.decode_string()
    if (key == '') {
      break
    }
    const value = this.do_decode()
    obj[key] = value
  }
  return obj
}

LoaderCodec.prototype.do_decode = function () {
  switch (this.read_byte()) {
    case 'u':
      return null
    case 'a':
      return this.decode_array()
    case 's':
      return this.decode_string()
    case 'x':
      return NaN
    case 'I':
      return Infinity
    case 'i':
      return -Infinity
    case 'n':
      const sign = this.read_byte()
      const value = this.decode_string()
      if (sign == 'u') {
        return -parseInt(value)
      } else {
        return parseInt(sign + value)
      }
    case 't':
      return true
    case 'f':
      return false
    case 'o':
      return this.decode_object()
    case 'j':
      return new Jump(parseInt(this.decode_string()))
    default:
      return null
  }
}

LoaderCodec.base64 = ':_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

module.exports = LoaderCodec
