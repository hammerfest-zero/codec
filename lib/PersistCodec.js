"use strict"

const BitCodec = require('./BitCodec')

/*
TYPE-BITS =

- number
	int 	00
	float	010
	NaN	0110
	+Inf	01110
	-Inf	01111

- array		100
- object	101
- boolean
	true	1101
	false	1100

- string	1110

- undefined 1111

DATA =
- int
	normal	NN [XX|XX|XX]  : 2 bits + 2-6 bits
	medium	1100 + 16 bits
	big	1101 + 32 bits
	negatif 111 + int

- float
	as a string with :
	string length	5
	character		4
        	number 0-9
		dot     10
		plus    11
		minus   12
		exp     13

- array
	element	0
	jump	10 + int
	end		11

- object
	key
		0 + index
		10 + string
	end
		11

- string
	extended	11
	ASCII		10
	B64		0
	len		int
	chars		 7/8
*/

function PersistCodec() {
  this.obfu_mode = false
  this.crc = false
  this.fast = false
}

PersistCodec.prototype.encode_array = function(a) {
  let njumps = 0
  a.forEach(v => {
    if (v == null)
      njumps++
    else {
      if (njumps > 0) {
        this.bc.write(2, 2)
        this.encode_int(njumps)
        njumps = 0
      }
      this.bc.write(1, 0)
      this.do_encode(v)
    }
  })
  this.bc.write(2, 3)
}

PersistCodec.prototype.decode_array = function() {
  const a = new Array()
  a.length = 0
  this.cache.unshift(a)
  return a
}

PersistCodec.prototype.decode_array_item = function(a) {
  const elt = (this.bc.read(1) == 0)
  if (elt) {
    a[a.length++] = this.do_decode()
  } else {
    const exit = (this.bc.read(1) == 1)
    if (exit)
      return false
    else
      a.length += this.decode_int()
  }
  return true
}

PersistCodec.prototype.decode_array_fast = function() {
  const a = new Array()
  while (true) {
    const elt = (this.bc.read(1) == 0)
    if (elt) {
      a[a.length++] = this.do_decode()
    } else {
      const exit = (this.bc.read(1) == 1)
      if (exit)
        break
      a.length += this.decode_int()
    }
    if (this.bc.hasError())
      break
  }
  return a
}

PersistCodec.prototype.encode_string = function(s) {
  let is_b64 = true
  let is_ascii = true

  for (let i = 0; i < s.length; ++i) {
    if (s.charCodeAt(i) > 127) {
      is_b64 = false
      is_ascii = false
      break
    } else if (is_b64) {
      if (BitCodec.d64(s[i]) == null)
        is_b64 = false
    }
  }

  this.encode_int(s.length)
  if (is_b64) {
    this.bc.write(1, 0)
    for (let i = 0; i < s.length; ++i) {
      this.bc.write(6, BitCodec.d64(s[i]))
    }
  } else {
    this.bc.write(2, is_ascii ? 2 : 3)
    for (let i = 0; i < s.length; ++i) {
      this.bc.write(is_ascii ? 7 : 8, s.charCodeAt(i))
    }
  }
}

PersistCodec.prototype.decode_string = function() {
  const len = this.decode_int()
  const is_b64 = (this.bc.read(1) == 0)
  let s = ""

  if (is_b64) {
    for (let i = 0; i < len; ++i) {
      s += BitCodec.c64(this.bc.read(6))
    }
  } else {
    const is_ascii = (this.bc.read(1) == 0)
    for (let i = 0; i < len; ++i) {
      s += BitCodec.chr(this.bc.read(is_ascii ? 7 : 8))
    }
  }
  return s
}

PersistCodec.prototype.encode_object = function(o) {
  for (let k in o) {
    this.encode_object_field(k, o[k])
  }
  this.bc.write(2, 3)
}

PersistCodec.prototype.encode_object_field = function(k, d) {
  if (typeof(d) != 'function' && d != null) {
    if (this.obfu_mode && k[0] == '$')
      k = k.substring(1)

    if (this.fields[k] != null) {
      this.bc.write(1, 0)
      this.bc.write(this.nfields_bits, this.fields[k])
    } else {
      this.fields[k] = this.nfields++
      if (this.nfields >= this.next_field_bits) {
        this.nfields_bits++
        this.next_field_bits *= 2
      }
      this.bc.write(2, 2)
      this.encode_string(k)
    }
    this.do_encode(d)
  }
}

PersistCodec.prototype.decode_object_fast = function() {
  const o = {}

  while (true) {
    let k
    const is_field_index = (this.bc.read(1) == 0)

    if (is_field_index) {
      k = this.fieldtbl[this.bc.read(this.nfields_bits)]
    } else {
      const is_end = (this.bc.read(1) == 1)
      if (is_end)
        break
      k = this.decode_string()
      if (this.obfu_mode && k[0] != '$')
        k = '$' + k
      this.fieldtbl[this.nfields++] = k
      if (this.nfields >= this.next_field_bits) {
        this.nfields_bits++
        this.next_field_bits *= 2
      }
    }
    o[k] = this.do_decode()
    if (this.bc.hasError())
      break
  }
  return o
}

PersistCodec.prototype.decode_object = function() {
  const o = {}
  this.cache.unshift(o)
  return o
}

PersistCodec.prototype.decode_object_field = function(o) {
  let k
  const is_field_index = (this.bc.read(1) == 0)

  if (is_field_index) {
    k = this.fieldtbl[this.bc.read(this.nfields_bits)]
  } else {
    const is_end = (this.bc.read(1) == 1)
    if (is_end)
      return false
    k = this.decode_string()
    if (this.obfu_mode && k[0] != '$')
      k = '$' + k
    this.fieldtbl[this.nfields++] = k
    if (this.nfields >= this.next_field_bits) {
      this.nfields_bits++
      this.next_field_bits *= 2
    }
  }
  o[k] = this.do_decode()
  return true
}

PersistCodec.prototype.encode_int = function(o) {
  if (o < 0) {
    this.bc.write(3, 7)
    this.encode_int(-o)
  } else if (o < 4) {
    this.bc.write(2, 0)
    this.bc.write(2, o)
  } else if (o < 16) {
    this.bc.write(2, 1)
    this.bc.write(4, o)
  } else if (o < 64) {
    this.bc.write(2, 2)
    this.bc.write(6, o)
  } else if (o < 65536) {
    this.bc.write(4, 12)
    this.bc.write(16, o)
  } else {
    this.bc.write(4, 13)
    this.bc.write(16, o & 0xFFFF)
    this.bc.write(16, (o >> 16) & 0xFFFF)
  }
}

PersistCodec.prototype.decode_int = function() {
  const nbits = this.bc.read(2)
  if (nbits == 3) {
    const is_neg = (this.bc.read(1) == 1)
    if (is_neg)
      return - this.decode_int()
    const is_big = (this.bc.read(1) == 1)
    if (is_big) {
      const n = this.bc.read(16)
      const n2 = this.bc.read(16)
      return n | (n2 << 16)
    } else {
      return this.bc.read(16)
    }
  }
  return this.bc.read((nbits + 1) * 2)
}

PersistCodec.prototype.encode_float = function(o) {
  const s = String(o)
  const l = s.length
  this.bc.write(5, l)

  for (let i = 0; i < l; ++i) {
    const c = s[i]
    if (c >= '0' && c <= '9')
      this.bc.write(4, s.charCodeAt(i) - 48)
    else if (c == '.')
      this.bc.write(4, 10)
    else if (c == '+')
      this.bc.write(4, 11)
    else if (c == '-')
      this.bc.write(4, 12)
    else
      this.bc.write(4, 13)
  }
}

PersistCodec.prototype.decode_float = function(o) {
  const l = this.bc.read(5)
  let s = ""

  for (let i = 0; i < l; ++i) {
    let k = this.bc.read(4)
    if (k < 10)
      k += 48
    else if (k == 10)
      k = 46
    else if (k == 11)
      k = 43
    else if (k == 12)
      k = 45
    else
      k = 101
    s += String.fromCharCode(k)
  }
  return parseFloat(s)
}

PersistCodec.prototype.do_encode = function(o) {
  if (o == null)
    this.bc.write(4, 15)
  else if (o instanceof Array) {
    this.bc.write(3, 4)
    this.encode_array(o)
  } else if (typeof o === 'string') {
    this.bc.write(4, 14)
    this.encode_string(o)
  } else if (typeof o === 'number') {
    const n = o
    if (isNaN(n))
      this.bc.write(4, 6)
    else if (n === Infinity)
      this.bc.write(5, 14)
    else if (n === -Infinity)
      this.bc.write(5, 15)
    else if (n === parseInt(n)) {
      this.bc.write(2, 0)
      this.encode_int(n)
    } else {
      this.bc.write(3, 2)
      this.encode_float(n)
    }
  } else if (typeof o === 'boolean') {
    this.bc.write(4, o === true ? 13 : 12)
  } else {
    this.bc.write(3, 5)
    this.encode_object(o)
  }
  return true
}

PersistCodec.prototype.do_decode = function() {
  const is_number = (this.bc.read(1) == 0)
  if (is_number) {
    const is_float = (this.bc.read(1) == 1)
    if (is_float) {
      const is_special = (this.bc.read(1) == 1)
      if (is_special) {
        const is_infinity = (this.bc.read(1) == 1)
        if (is_infinity) {
          const is_negative = (this.bc.read(1) == 1)
          return (is_negative ? -Infinity : Infinity)
        } else {
          return NaN
        }
      } else {
        return this.decode_float()
      }
    } else {
      return this.decode_int()
    }
  }

  const is_array_obj = (this.bc.read(1) == 0)
  if (is_array_obj) {
    const is_obj = (this.bc.read(1) == 1)
    if (is_obj) {
      return this.fast ? this.decode_object_fast() : this.decode_object()
    } else {
      return this.fast ? this.decode_array_fast() : this.decode_array()
    }
  }

  const tflag = this.bc.read(2)
  if (tflag == 0)
    return false
  else if (tflag == 1)
    return true
  else if (tflag == 2)
    return this.decode_string()
  else return null
}

PersistCodec.prototype.encodeInit = function(o) {
  this.bc = new BitCodec()
  this.fields = {}
  this.nfields = 0
  this.next_field_bits = 1
  this.nfields_bits = 0
  this.cache = new Array()
  this.cache.push(o)
}

PersistCodec.prototype.encodeLoop = function() {
  if (this.cache.length == 0)
    return true
  this.do_encode(this.cache.shift())
  return false
}

PersistCodec.prototype.encodeEnd = function() {
  let s = this.bc.toString()
  if (this.crc)
    s += this.bc.crcStr()
  return s
}

PersistCodec.prototype.encode = function(o) {
  this.encodeInit(o)
  while (this.encodeLoop()) {}
  return this.encodeEnd()
}

PersistCodec.prototype.decodeInit = function(data) {
  this.bc = new BitCodec(data)
  this.fieldtbl = new Array()
  this.nfields = 0
  this.next_field_bits = 1
  this.nfields_bits = 0
  this.cache = new Array()
  this.result = null
}

PersistCodec.prototype.decodeLoop = function() {
  if (this.cache.length == 0)
    this.result = this.do_decode()
  else {
    const o = this.cache[0]
    if (o instanceof Array) {
      if (!this.decode_array_item(o))
        this.cache.shift()
    } else {
      if (!this.decode_object_field(o)) {
        delete o.pos
        this.cache.shift()
      }
    }
  }
  if (this.bc.hasError()) {
    this.result = null
    return false
  }
  return (this.cache.length != 0)
}

PersistCodec.prototype.decodeEnd = function() {
  if (this.crc) {
    const s = this.bc.crcStr()
    const s2 = this.bc.data.substr(this.bc.in_pos, 4)
    if (s != s2)
      return null
  }
  return this.result
}

PersistCodec.prototype.decode = function(data) {
  this.decodeInit(data)
  while (this.decodeLoop()) {}
  return this.decodeEnd()
}

module.exports = PersistCodec
