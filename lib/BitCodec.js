"use strict"

function BitCodec(d) {
  this.setData(d || "")
}

BitCodec.prototype.setData = function(d) {
  this.error_flag = false
  this.data = d
  this.in_pos = 0
  this.nbits = 0
  this.bits = 0
  this.crc = 0
}

BitCodec.prototype.crcStr = function() {
  return BitCodec.c64(this.crc & 63)
    + BitCodec.c64((this.crc >> 6) & 63)
    + BitCodec.c64((this.crc >> 12) & 63)
    + BitCodec.c64((this.crc >> 18) & 63)
}

BitCodec.prototype.read = function(n) {
  while (this.nbits < n) {
    const c = BitCodec.d64(this.data[this.in_pos++])
    if (this.in_pos > this.data.length
        || c == null ) {
      this.error_flag = true
      return -1
    }
    this.crc ^= c
    this.crc &= 0xFFFFFF
    this.crc *= c
    this.nbits += 6
    this.bits <<= 6
    this.bits |= c
  }
  this.nbits -= n
  return (this.bits >> this.nbits) & ((1 << n) - 1)
}

BitCodec.prototype.nextPart = function() {
  this.nbits = 0
}

BitCodec.prototype.hasError = function() {
  return this.error_flag
}

BitCodec.prototype.toString = function() {
  if (this.nbits > 0)
    this.write(6 - this.nbits, 0)
  return this.data
}

BitCodec.prototype.write = function(n, b) {
  this.nbits += n
  this.bits <<= n
  this.bits |= b
  while (this.nbits >= 6) {
    this.nbits -= 6
    const k = (this.bits >> this.nbits) & 63
    this.crc ^= k
    this.crc &= 0xFFFFFF
    this.crc *= k
    this.data += BitCodec.c64(k)
  }
}

BitCodec.ord = function(code) {
  return code.charCodeAt(0)
}

BitCodec.chr = function(code) {
  return String.fromCharCode(code)
}

BitCodec.d64 = function(code) {
  if (code >= 'a' && code <= 'z')
    return BitCodec.ord(code) - BitCodec.ord('a')
  else if (code >= 'A' && code <= 'Z')
    return BitCodec.ord(code) - BitCodec.ord('A') + 26
  else if (code >= '0' && code <= '9')
    return BitCodec.ord(code) - BitCodec.ord('0') + 52
  else if (code == '-')
    return 62
  else if (code == '_')
    return 63
  else
    return null
}

BitCodec.c64 = function(code) {
  if (code < 0)
    return '?'
  else if (code < 26)
    return BitCodec.chr(code + BitCodec.ord('a'))
  else if (code < 52)
    return BitCodec.chr(code - 26 + BitCodec.ord('A'))
  else if (code < 62)
    return BitCodec.chr(code - 52 + BitCodec.ord('0'))
  else if (code == 62)
    return '-'
  else if (code == 63)
    return '_'
  else
    return '?'
}

module.exports = BitCodec
