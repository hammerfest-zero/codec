const test = require('ava')
const { LoaderCodec } = require('../..')

test('encode - empty array', t => {
  t.is((new LoaderCodec()).encode([]), 'a0:3V899')
})

test('encode - integers array', t => {
  t.is((new LoaderCodec()).encode([1, 2, 3]), 'a3:n1:n2:n3:wF6PS')
})

test('encode - strings array', t => {
  t.is((new LoaderCodec()).encode(['foo', 'bar']), 'a2:sfoo:sbar:iEbY1')
})

test('encode - mixed array', t => {
  t.is((new LoaderCodec()).encode([1, null, 'bar']), 'a3:n1:usbar:Z9Xjl')
})

/* null values are merged */
test('encode - null array', t => {
  t.is((new LoaderCodec()).encode([null, null, null, null, null]), 'a5:j5:D5BEJ')
})

test('encode - null and values array 1', t => {
  t.is((new LoaderCodec()).encode([null, null, null, null, null, 5]), 'a6:j5:n5:hV9bA')
})

test('encode - nested null arrays', t => {
  t.is((new LoaderCodec()).encode([1, [null, null, null]]), 'a2:n1:a3:j3:R2Iwk')
})

test('decode - empty array', t => {
  t.deepEqual((new LoaderCodec()).decode('a0:3V899'), [])
})

test('decode - integers array', t => {
  t.deepEqual((new LoaderCodec()).decode('a3:n1:n2:n3:wF6PS'), [1, 2, 3])
})

test('decode - strings array', t => {
  t.deepEqual((new LoaderCodec()).decode('a2:sfoo:sbar:iEbY1'), ['foo', 'bar'])
})

test('decode - mixed array', t => {
  t.deepEqual((new LoaderCodec()).decode('a3:n1:usbar:Z9Xjl'), [1, null, 'bar'])
})

/* there is no differences between `null` and `undefined` within this encoder */
test('decode - null and values array 1', t => {
  t.deepEqual((new LoaderCodec()).decode('a5:j5:D5BEJ'), [undefined, undefined, undefined, undefined, undefined])
})

test('decode - null and values array 2', t => {
  t.deepEqual((new LoaderCodec()).decode('a6:j5:n5:hV9bA'), [undefined, undefined, undefined, undefined, undefined, 5])
})

test('decodeencode - nested null arrays', t => {
  t.deepEqual((new LoaderCodec()).decode('a2:n1:a3:j3:R2Iwk'), [1, [undefined, undefined, undefined]])
})
