const test = require('ava')
const { LoaderCodec } = require('../..')

test('encode - negative', t => {
  t.is((new LoaderCodec()).encode(-42), 'nu42:YGhS3')
})

test('encode - 0', t => {
  t.is((new LoaderCodec()).encode(0), 'n0:IF099')
})

test('encode - positive', t => {
  t.is((new LoaderCodec()).encode(42), 'n42:vyFf:')
})

test('encode - floating number (int parsed)', t => {
  t.is((new LoaderCodec()).encode(42.5), 'n42:vyFf:')
})

test('decode - negative', t => {
  t.is((new LoaderCodec()).decode('nu42:YGhS3'), -42)
})

test('decode - 0', t => {
  t.is((new LoaderCodec()).decode('n0:IF099'), 0)
})

test('decode - positive', t => {
  t.is((new LoaderCodec()).decode('n42:vyFf:'), 42)
})
