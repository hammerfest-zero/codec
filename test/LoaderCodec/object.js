const test = require('ava')
const { LoaderCodec } = require('../..')

test('encode - empty object', t => {
  t.is((new LoaderCodec()).encode({}), 'o:Gl:::')
})

test('encode - simple object', t => {
  t.is((new LoaderCodec()).encode({'foo': 'bar'}), 'ofoo:sbar::4QVXc')
})

test('decode - empty object', t => {
  t.deepEqual((new LoaderCodec()).decode('o:Gl:::'), {})
})

test('decode - simple object', t => {
  t.deepEqual((new LoaderCodec()).decode('ofoo:sbar::4QVXc'), {'foo': 'bar'})
})
