const test = require('ava')
const { LoaderCodec } = require('../..')

test('encode - true', t => {
  t.is((new LoaderCodec('')).encode(true), 'tO9999')
})

test('encode - false', t => {
  t.is((new LoaderCodec('')).encode(false), 'f29999')
})

test('decode - true', t => {
  t.is((new LoaderCodec('')).decode('tO9999'), true)
})

test('decode - false', t => {
  t.is((new LoaderCodec('')).decode('f29999'), false)
})
