const test = require('ava')
const { LoaderCodec } = require('../..')

test('encode - empty string', t => {
  t.is((new LoaderCodec('')).encode(''), 's:So:::')
})

test('encode - "foobar"', t => {
  t.is((new LoaderCodec('')).encode('foobar'), 'sfoobar:lZo7K')
})

// Encoding working with ":", but the messages will be undecodeable
test('encode - with ":" - "foo:bar"', t => {
  t.is((new LoaderCodec('')).encode('foo:bar'), 'sfoo:bar:g:Qx8')
})

test('encode - non alphanum - "éléphant"', t => {
  t.is((new LoaderCodec('')).encode('éléphant'), 'séléphant:1rwyN')
})

test('encode - non alphanum (with crypto) - "éléphant"', t => {
  t.is((new LoaderCodec('masterkey')).encode('éléphant'), '_édéHhFi1kUaeuT')
})

test('decode - empty string', t => {
  t.is((new LoaderCodec('')).decode('s:So:::'), '')
})

test('decode - "foobar"', t => {
  t.is((new LoaderCodec('')).decode('sfoobar:lZo7K'), 'foobar')
})

// Incorrect decoding with ":"
test('decode - with ":" - "foo:bar"', t => {
  t.is((new LoaderCodec('')).decode('sfoo:bar:g:Qx8'), 'foo')
})

test('decode - non alphanum - "éléphant"', t => {
  t.is((new LoaderCodec('')).decode('séléphant:1rwyN'), 'éléphant')
})

// non alphanum characteres are not encrypted
test('decode - non alphanum (with crypto) - "éléphant"', t => {
  t.is((new LoaderCodec('masterkey')).decode('_édéHhFi1kUaeuT'), 'éléphant')
})
