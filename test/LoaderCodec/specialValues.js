const test = require('ava')
const { LoaderCodec } = require('../..')

test('encode - Infinity', t => {
  t.is((new LoaderCodec()).encode(Infinity), 'Iz9999')
})

test('encode - -Infinity', t => {
  t.is((new LoaderCodec()).encode(-Infinity), 'iZ9999')
})

test('encode - NaN', t => {
  t.is((new LoaderCodec()).encode(NaN), 'xK9999')
})

test('encode - null', t => {
  t.is((new LoaderCodec()).encode(null), 'uN9999')
})

test('encode - undefined', t => {
  t.is((new LoaderCodec()).encode(undefined), 'uN9999')
})

test('encode - function (invalid)', t => {
  t.is((new LoaderCodec()).encode(new Function()), 'o:Gl:::')
})

test('decode - Infinity', t => {
  t.is((new LoaderCodec()).decode('Iz9999'), Infinity)
})

test('decode - -Infinity', t => {
  t.is((new LoaderCodec()).decode('iZ9999'), -Infinity)
})

test('decode - NaN', t => {
  t.is((new LoaderCodec()).decode('xK9999'), NaN)
})

test('decode - null', t => {
  t.is((new LoaderCodec()).decode('uN9999'), null)
})

test('decode - invalid type', t => {
  t.is((new LoaderCodec()).decode('Fw9999'), null)
})
