const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - empty array', t => {
  t.is((new PersistCodec()).encode([]), 'M')
})

test('encode - integers array', t => {
  t.is((new PersistCodec()).encode([1, 2, 3]), 'GeedW')
})

test('encode - strings array', t => {
  t.is((new PersistCodec()).encode(['foo', 'bar']), 'JJcNhomcaJG')
})

test('encode - mixed array', t => {
  t.is((new PersistCodec()).encode([1, 'bar']), 'GfXGqeC')
})

/* an array full of `null` is treated as an empty array */
test('encode - null array', t => {
  t.is((new PersistCodec()).encode([null, null, null, null, null]), 'M')
})

test('encode - null and values array 1', t => {
  t.is((new PersistCodec()).encode([null, null, null, null, 5]), 'KOfC')
})

test('encode - null and values array 2', t => {
  t.is((new PersistCodec()).encode([null, null, null, null, null, 5]), 'KQfC')
})

/* trailing `null` values are ignored */
test('encode - null and values array with trailing null', t => {
  t.is((new PersistCodec()).encode([null, null, null, null, null, 5, null]), 'KQfC')
})

test('decode - empty array', t => {
  t.deepEqual((new PersistCodec()).decode('M'), [])
})

test('decode - integers array', t => {
  t.deepEqual((new PersistCodec()).decode('GeedW'), [1, 2, 3])
})

test('decode - strings array', t => {
  t.deepEqual((new PersistCodec()).decode('JJcNhomcaJG'), ['foo', 'bar'])
})

test('decode - mixed array', t => {
  t.deepEqual((new PersistCodec()).decode('GfXGqeC'), [1, 'bar'])
})

/* there is no differences between `null` and `undefined` within this encoder */
test('decode - null and values array 1', t => {
  t.deepEqual((new PersistCodec()).decode('KOfC'), [undefined, undefined, undefined, undefined, 5])
})

test('decode - null and values array 2', t => {
  t.deepEqual((new PersistCodec()).decode('KQfC'), [undefined, undefined, undefined, undefined, undefined, 5])
})
