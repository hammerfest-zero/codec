const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - empty object', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.is(codec.encode({}), 'U')
})

test('encode - simple object', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.is(codec.encode({'foo': 'bar'}), 'Syu47JaGi4')
})

test('decode - empty object', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.deepEqual(codec.decode('U'), {})
})

test('decode - simple object', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.deepEqual(codec.decode('Syu47JaGi4'), {'foo': 'bar'})
})
