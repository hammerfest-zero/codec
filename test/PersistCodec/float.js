const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - negative', t => {
  t.is((new PersistCodec()).encode(-1.5), 'rmgL')
})

test('encode - positive', t => {
  t.is((new PersistCodec()).encode(1.5), 'qXPq')
})

test('encode - really big positive number', t => {
  t.is((new PersistCodec()).encode(1.5e24), 'rXPDSK')
})

test('encode - really small positive number', t => {
  t.is((new PersistCodec()).encode(1.5e-24), 'rXPDWK')
})

test('decode - negative', t => {
  t.is((new PersistCodec()).decode('rmgL'), -1.5)
})

test('decode - positive', t => {
  t.is((new PersistCodec()).decode('qXPq'), 1.5)
})

test('decode - really big positive number', t => {
  t.is((new PersistCodec()).decode('rXPDSK'), 1.5e24)
})

test('decode - really small positive number', t => {
  t.is((new PersistCodec()).decode('rXPDWK'), 1.5e-24)
})
