const test = require('ava')
const { PersistCodec } = require('../..')

/* Those two tests feature the first level of the original game */

const encoded = 'S3joEpFYY09M_PZNoYtNWY49ohzt2B-NoCXjmwhjm08-K5EsC8FltW6DGjXtXjH6DmopqGsCSVdlH6pus1IIFducbaGEjmT_rPYzuctfV5zmVlM9rmusbj007mVn6IyOL74AGqiedXjLV7CAbjUEOKM3lU6OKctXYEOMcjaK88NQjGIqjod1eWrieMldJ1VutfeGsC9or6IySes98nqiecb4KY39UnaK3pustrH27CVk_HX48VpNV5ACVneGsEot1eWrieNNK9rmesbjZ05hQjIIxVHRXEKW9oMhhOFdrRXjU07Twf8nqiecb4K6x-MNBL5ieNr6IzSKt4AGqiecbaGqpeMw_T550cttKEOMlvWIxVHQbaGqpf6tPF6ADUxK-gVf6tDP3ASl4AnEjoL_HV5OeNH6IyUesbj5EOMlLeGszhQjOIqjnExY9rout4AGqiedXjLV7CAbjUEOKM3lU6Xn-_KIqjphj6IzT2IjaK88NQjOWzOKctG9rmesbjIW49B1eXrl3W1aGqihItlF240ctC9rjmVJHS389o_CIqjphj6IzUwcjaK88NQjM4yiKctG9rmus98nqiecb4KY7UMxKGsEhQjI5rieNL6IyUut4AGqiecbaGqpeMw_W38UMhyGsCnor6IyiL74AGqiedXEKY7UMxK-gVf6tPF4B-B4AnEjmVJGGsEhQjIXrieNL6IyUut4AGqiedXjLV69oZzq6if74AGqiedXjLV8nUnaK56CJ1eXrl3W1aGqihItlF5zDQbj4yVutfIIqjplf6IyUusbj4zputfIIqjplj6IyUus98nqiecb4KY39VpoGsACJ1eXzmKs98nqiecb4KY38FtLPZ58VjaK05hQjGIxVHQbaGqpeMw_HYzectWXEONkjaK8SxQjKWrieNHK9rousbj5zputjGIqjoNNHLEOMcjE-gVf6tl44pHO14K6x-MNBL5ieNr6IyTut4AGqiedXjLV7EEDaK05hQjIWyiL74AGqiedXjLV8n-ND0Y8KctW9roesbj5EOMlreVFdxI9j0V9no3lYFdrRXj0V9no3lYqjoJ1e4AONW1aGqihItlF16DMYH0ql3W14VsDl_ttTY8NW0A8sDl_dFZqjpd1eXCiKctY9rn0sbjKEOMAjaK5zEgxd0EOLRfe-gOecba8szB-VtS2tEEDaVFdxI9j0V8n_n8ngVeNs_W380ctW9rmKsbj5EOMYjaKYputreGsCSVdlH6pus1IIFducbaGEjmT_xP2BjVpoGxVHRXEK6x-g_M-gJxItPF4B-Abj4EOMsjaK8VutbeGszhQjKIFducbaGEjmT_NJ38SQbjPEOMRfeGsC9l1eWrieNH6ICOKctY9rnKsbjP3zNQjIIxVHRXEK6x-g_M-gJxItPF4B-Abj4EONcjaK8VutjIIqjmJ1eXrpHQbaGqpeMw_T550cttKEOMlbIIxVHQbaGqpeMw_W36D3tlYqjpd1eYrieNL6Iznes98nEl0Ns_W383W0BlYtNR07OwCEMPjZ16D1ttSY8YqMjjW2yFoxLzjoyKNdzH85EvGguKM3dWIaaaaabaaaiaabaaaiaaaaaaaeaaaanaaaaaaGaaeaaaGaaeaaaaaiacbaGqoGaaaaaqaacaaaaaacaaaaaeabaaaadqaaaaaiaabaaaaaaaaaaaacaaGaaedOaaaaaaaaaaaaaaaaaaaaaaaaqaaca0aaaaaaaaaaaaaaaaaaaaaaaaiaabaAaaaaaaaaaiaaaaaaaaaaaaaaeaaaGnaaaaaaaaaeaaaaaaaaaaaaiecaaaqgGaaaaaaaacaaaaaaaaaaaaeaaaaqihqaaaaaaaabaaaaaabaaaiecbaaqaabOaaaaaaaaaGaaaaaaGaaaaaaaaaaaa0aaaaaaaaaqaaaaaaqaaaaaaqaaaaaAaaaaaaaaaiaaaaaaiaaaaaaaaaaaanaaaaaaaaaaaaaaaaeaaaGqiecbaaagGaaaaaaaaaaaaaaaaaaaaaeaaaGqihqaaaaaiaabaaaaaaaaaaaacbaGaaabObaaaiecbaGqiecbaGqaaaaaaqaaaa0aaaaaabaaaiaabaaaiaaaaaaiaaaaAaaaaaaaGaaeaaaGaaeaaaGaaeaaaanaaaaaaaaaaaaaaaaaaaaaqaacaaaah4'

const decoded = {
  '$scoreSlots': [],
  '$specialSlots': [],
  '$badList': [],
  '$script':
  '<$attach $repeat="-1">\r    <$e_hide $borders="1" $tiles="1"/>\r    <$e_mc $n="$menu" $xr="0" $yr="0" $p="0" $back="1" $sid="10"/>\r    <$e_mc $n="$hammer_accessories" $xr="0" $yr="0" $sid="1"/>\r</$attach>\r\r<$ninja>\r    <$t_timer $t="32">\r        <$e_msg $id="158"/>\r    </$t_timer>\r</$ninja>\r\r<$t_pos $x="18" $y="19" $d="4" $key="9">\r    <$e_mc $n="$menuDoor" $xr="374" $yr="403" $p="0" $back="1"/>\r    <$e_mc $n="$explosion" $xr="390" $yr="380" $p="1"/>\r    <$enter $x="19" $y="19">\r        <$e_portal $pid="0"/>\r    </$enter>\r</$t_pos>\r\r<$exp $x="11" $y="19">\r    <$e_killPt />\r    <$e_pmc $sid="1"/>\r    <$e_rem $x1="11" $y1="19" $x2="11" $y2="19"/>\r    <$e_msg $id="122"/>\r    <$e_ctrigger $id="0"/>\r    <$e_add $x1="9" $y1="20" $x2="9" $y2="20" $type="0"/>\r</$exp>\r\r<$t_timer $t="15">\r    <$e_msg $id="100"/>\r    <$e_pointer $x="8" $y="14"/>\r</$t_timer>\r\r<$t_timer $t="85">\r    <$e_killPt />\r</$t_timer>\r\r<$t_pos $x="18" $y="7" $d="3" $repeat="-1">\r    <$e_killMsg />\r</$t_pos>\r\r<$t_pos $x="2" $y="6" $d="4" $repeat="-1">\r    <$e_killMsg />\r</$t_pos>\r\r<$t_pos $x="2" $y="0" $d="2">\r    <$e_score $i="51" $si="0" $x="9" $y="6" $inf="1"/>\r</$t_pos>\r\r<$t_pos $x="8" $y="21" $d="1">\r    <$e_msg $id="101"/>\r    <$e_pointer $x="2" $y="24"/>\r</$t_pos>\r\r',
  '$skinBg': 14,
  '$skinTiles': 16,
  '$playerY': 19,
  '$playerX': 1,
  '$map':
  [ [ 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1 ],
    [ 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1 ],
    [ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1 ],
    [ 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 ],
    [ 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0 ] ] }


test('encode - complete level', t => {
  t.is((new PersistCodec()).encode(decoded), encoded)
})

test('encode with crc - complete level', t => {
  const codec = new PersistCodec()
  codec.crc = true
  t.is(codec.encode(decoded), encoded + '4haa')
})

test('encode_fast - complete level', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.is(codec.encode(decoded), encoded)
})

test('decode - complete level', t => {
  t.deepEqual((new PersistCodec()).decode(encoded), decoded)
})

test('decode with crc - complete level', t => {
  const codec = new PersistCodec()
  codec.crc = true
  t.deepEqual(codec.decode(encoded + '4haa'), decoded)
})

test('decode_fast - complete level', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.deepEqual(codec.decode(encoded), decoded)
})
