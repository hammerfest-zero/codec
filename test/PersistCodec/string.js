const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - empty string', t => {
  t.is((new PersistCodec()).encode(''), '4a')
})

test('encode - "foobar"', t => {
  t.is((new PersistCodec()).encode('foobar'), '5ykCCcaI')
})

test('encode with crc - "foobar"', t => {
  const codec = new PersistCodec()
  codec.crc = true
  t.is(codec.encode('foobar'), '5ykCCcaIesaa')
})

test('encode - extended ascii - "éléphant"', t => {
  t.is((new PersistCodec()).encode('éléphant'), '5J6wZPCgHHBNq')
})

test('decode - empty string', t => {
  t.is((new PersistCodec()).decode('4a'), '')
})

test('decode - "foobar" bits', t => {
  t.is((new PersistCodec()).decode('5ykCCcaI'), 'foobar')
})

test('decode with crc - "foobar"', t => {
  const codec = new PersistCodec()
  codec.crc = true
  t.is(codec.decode('5ykCCcaIesaa'), 'foobar')
})

test('decode with invalid crc - "foobar"', t => {
  const codec = new PersistCodec()
  codec.crc = true
  t.is(codec.decode('5ykCCcaIesab'), null)
})

test('decode - extended ascii - "éléphant"', t => {
  t.is((new PersistCodec()).decode('5J6wZPCgHHBNq'), 'éléphant')
})
