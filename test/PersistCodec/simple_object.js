const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - empty object', t => {
  t.is((new PersistCodec()).encode({}), 'U')
})

test('encode - simple object', t => {
  t.is((new PersistCodec()).encode({'foo': 'bar'}), 'Syu47JaGi4')
})

test('decode - empty object', t => {
  t.deepEqual((new PersistCodec()).decode('U'), {})
})

test('decode - simple object', t => {
  t.deepEqual((new PersistCodec()).decode('Syu47JaGi4'), {'foo': 'bar'})
})
