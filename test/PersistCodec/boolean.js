const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - true', t => {
  t.is((new PersistCodec()).encode(true), '0')
})

test('encode - false', t => {
  t.is((new PersistCodec()).encode(false), 'W')
})

test('decode - true', t => {
  t.is((new PersistCodec()).decode('0'), true)
})

test('decode - false', t => {
  t.is((new PersistCodec()).decode('W'), false)
})
