const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - Infinity', t => {
  t.is((new PersistCodec()).encode(Infinity), 'C')
})

test('encode - -Infinity', t => {
  t.is((new PersistCodec()).encode(-Infinity), 'E')
})

test('encode - NaN', t => {
  t.is((new PersistCodec()).encode(NaN), 'y')
})

test('encode - null', t => {
  t.is((new PersistCodec()).encode(null), '8')
})

test('encode - undefined', t => {
  t.is((new PersistCodec()).encode(undefined), '8')
})

test('encode - function (invalid)', t => {
  t.is((new PersistCodec()).encode(new Function()), 'U')
})

test('decode - Infinity', t => {
  t.is((new PersistCodec()).decode('C'), Infinity)
})

test('decode - -Infinity', t => {
  t.is((new PersistCodec()).decode('E'), -Infinity)
})

test('decode - NaN', t => {
  t.is((new PersistCodec()).decode('y'), NaN)
})

test('decode - null', t => {
  t.is((new PersistCodec()).decode('8'), null)
})
