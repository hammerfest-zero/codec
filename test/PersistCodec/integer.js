const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - negative', t => {
  t.is((new PersistCodec()).encode(-42), 'pva')
})

test('encode - 2 bits', t => {
  t.is((new PersistCodec()).encode(3), 'd')
})

test('encode - 4 bits', t => {
  t.is((new PersistCodec()).encode(15), 'hW')
})

test('encode - 6 bits', t => {
  t.is((new PersistCodec()).encode(42), 'kO')
})

test('encode - 16 bits', t => {
  t.is((new PersistCodec()).encode(30000), 'mDta')
})

test('encode - 32 bits', t => {
  t.is((new PersistCodec()).encode(300000), 'nK-aaba')
})

test('decode - negative', t => {
  t.is((new PersistCodec()).decode('pva'), -42)
})

test('decode - 2 bits', t => {
  t.is((new PersistCodec()).decode('d'), 3)
})

test('decode - 4 bits', t => {
  t.is((new PersistCodec()).decode('hW'), 15)
})

test('decode - 6 bits', t => {
  t.is((new PersistCodec()).decode('kO'), 42)
})

test('decode - 16 bits', t => {
  t.is((new PersistCodec()).decode('mDta'), 30000)
})

test('decode - 32 bits', t => {
  t.is((new PersistCodec()).decode('nK-aaba'), 300000)
})

