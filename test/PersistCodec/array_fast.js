const test = require('ava')
const { PersistCodec } = require('../..')

test('encode - empty array', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.is(codec.encode([]), 'M')
})

test('encode - integers array', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.is(codec.encode([1, 2, 3]), 'GeedW')
})

test('encode - strings array', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.is(codec.encode(['foo', 'bar']), 'JJcNhomcaJG')
})

test('encode - mixed array', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.is(codec.encode([1, 'bar']), 'GfXGqeC')
})

test('decode - empty array', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.deepEqual(codec.decode('M'), [])
})

test('decode - integers array', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.deepEqual(codec.decode('GeedW'), [1, 2, 3])
})

test('decode - strings array', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.deepEqual(codec.decode('JJcNhomcaJG'), ['foo', 'bar'])
})

test('decode - mixed array', t => {
  const codec = new PersistCodec()
  codec.fast = true
  t.deepEqual(codec.decode('GfXGqeC'), [1, 'bar'])
})
