# Codecs

A collection of codecs used in Hammerfest.

```sh
npm install git+https://gitlab.com/hammerfest-zero/codec.git#v0.2
```

## PersistCodec

This codec is used to encode and decode ingame levels. It turns a javascript object into a 6bits encoded string. A mtypes version of this codec can be found [here](https://gitlab.com/hammerfest-zero/mtypes/-/blob/master/lib/PersistCodec.mt).

```js
const { PersistCodec } = require('codec')

const codec = new PersistCodec()
codec.encode({'foo': 'bar'}) // => 'Syu47JaGi4'
codec.decode('Syu47JaGi4') // => {'foo': 'bar'}
```

Note that the behaviour of the `encode` and `decode` methods can be modified by setting `codec.fast`, `codec.obfu_mode` or `codec.crc` to `true`.  
While this codec is rather stable, some bugs might still occur (code coverage is about 95% as of v0.1). Feel free to fill an issue if you get into any trouble.

## BitCodec

This codec is the backend codec used by [PersistCodec](#PersistCodec). It basically is a base64 codec with an optional checksum. A mtypes version of this codec can be found [here](https://gitlab.com/hammerfest-zero/mtypes/-/blob/master/lib/BitCodec.mt).

## LoaderCodec

This codec is used by the loader of the game to send game results to the server, with optional encryption and a mandatory checksum. This one comes with a few limitations :  

- It is impossible to encode floats with this codec (floats are converted to integers)
- The character `:` is actually the end-of-string character, meaning it cannot be used in a string nor in an object key
- Only alphanumeric and `_` characters support encryption. Any other character will remain plain text

```js
const { LoaderCodec } = require('codec')

// Without encryption
const codec = new LoaderCodec()
codec.encode({'foo': 'bar'})  // => 'ofoo:sbar::4QVXc'
codec.decode('ofoo:sbar::4QVXc') // => {'foo': 'bar'}

// With encryption
const encCodec = new LoaderCodec('secretkey')
encCodec.encode({'foo': 'bar'})  // => 'vOX1gd24yaYnkCGL'
encCodec.decode('vOX1gd24yaYnkCGL') // => {'foo': 'bar'}
encCodec.encode('super string with non-ascii, and utf-8 characters (like this unicorn 🦄) that cannot be encrypted, thus will remain plain text') // => 'r3Z0ma TRyg5e eMqI Q1f-rT6bg, 0_z YqO-B H_rM4lvWBi (TlFJ W_zT SegUClR 🦄) qIFW krY1xv 1g wRb830Bv8, R:sE usTi 8JNizY Nc:8_ jIC2HgeZxg'
```

Note that, as of v0.2, the checksum isn't checked while decoding.
