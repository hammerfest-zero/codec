# Changelog

## 0.2.0 - 2020-07-11

### Added

- The LoaderCodec codec
- The BitCodec codec is now available using `require()`

### Fixed

- Fix an error encoding in PersistCodec using complex types (as objects arrays)

### Removed

- Method `PersistCodec.prototype.getProgress()` was useless

## 0.1.0 - 2020-05-24

### Added

- The PersistCodec codec
- The BitCodec codec, non exposed to the public API
